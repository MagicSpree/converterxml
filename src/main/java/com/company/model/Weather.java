package com.company.model;

public class Weather {

    private final Temperature temperature;
    private long humidity;
    private String weatherCondition;
    private final City city;

    public Weather() {
        city = new City();
        temperature = new Temperature();
    }

    public City getCity() {
        return city;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setHumidity(long humidity) {
        this.humidity = humidity;
    }

    public void setWeatherCondition(String weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    public long getHumidity() {
        return humidity;
    }

    public String getWeatherCondition() {
        return weatherCondition;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "Температура=" + temperature.temp +
                ", Единица измерения температуры='" + temperature.unit + '\'' +
                ", Влажность=" + humidity +
                ", Погодные условия='" + weatherCondition + '\'' +
                ", Город='" + city.nameCity + '\'' +
                ", Страна='" + city.country + '\'' +
                '}';
    }

    public class City {
        private String nameCity;
        private String country;

        public void setNameCity(String nameCity) {
            this.nameCity = nameCity;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getNameCity() {
            return nameCity;
        }

        public String getCountry() {
            return country;
        }
    }

    public class Temperature {
        private long temp;
        private String unit;

        public void setTemp(long temp) {
            this.temp = temp;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public long getTemp() {
            return temp;
        }

        public String getUnit() {
            return unit;
        }
    }
}
