package com.company.java;

import com.company.model.Weather;
import org.json.JSONObject;

public class JSONMapper {

    public String buildWeatherJson(Weather weather) {
        JSONObject weatherArray = new JSONObject();

        JSONObject infoCityObj = new JSONObject();
        infoCityObj.put("name", weather.getCity().getNameCity());
        infoCityObj.put("country", weather.getCity().getCountry());
        weatherArray.append("city", infoCityObj);

        JSONObject infoTempObj = new JSONObject();
        infoTempObj.put("value", weather.getTemperature().getTemp());
        infoTempObj.put("unit", weather.getTemperature().getUnit());
        weatherArray.append("temperature", infoTempObj);

        weatherArray.put("humidity", weather.getHumidity());
        weatherArray.put("weatherCondition", weather.getWeatherCondition());

        System.out.println(weatherArray);

        return weatherArray.toString();
    }
}
