package com.company.xmlparse;

import com.company.model.Weather;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLMapperHandler extends DefaultHandler {
    private static final String CITY = "city";
    private static final String NAME_CITY = "name";
    private static final String COUNTRY = "country";
    private static final String TEMPERATURE = "temperature";
    private static final String VALUE_TEMP = "value";
    private static final String UNIT = "unit";
    private static final String HUMIDITY = "humidity";
    private static final String WEATHER = "weather";

    private String currentTagName;
    private Weather weather;

    @Override
    public void startDocument() throws SAXException {
        weather = new Weather();
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentTagName = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentTagName = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentTagName == null)
            return;
        String elementValue = new String(ch, start, length);
        switch (currentTagName) {
            case NAME_CITY:
                weather.getCity().setNameCity(elementValue);
                break;
            case COUNTRY:
                weather.getCity().setCountry(elementValue);
                break;
            case VALUE_TEMP:
                weather.getTemperature().setTemp(Long.parseLong(elementValue));
                break;
            case UNIT:
                weather.getTemperature().setUnit(elementValue);
                break;
            case HUMIDITY:
                weather.setHumidity(Long.parseLong(elementValue));
                break;
            case WEATHER:
                weather.setWeatherCondition(elementValue);
                break;
        }
    }

    public Weather getWeather() {
        return weather;
    }
}
