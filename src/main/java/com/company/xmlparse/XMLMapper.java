package com.company.xmlparse;


import com.company.model.Weather;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class XMLMapper{

    public Weather parse() throws Exception{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        XMLMapperHandler xmlMapperHandler = new XMLMapperHandler();
        SAXParser parser = factory.newSAXParser();
        File file = new File("weather.xml");
        parser.parse(file, xmlMapperHandler);
        return xmlMapperHandler.getWeather();
    }
}
