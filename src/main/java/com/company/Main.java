package com.company;

import com.company.java.JSONMapper;
import com.company.model.Weather;
import com.company.xmlparse.XMLMapper;

public class Main {

    public static void main(String[] args) {
        XMLMapper xmlMapper = new XMLMapper();

        try {
            Weather weather = xmlMapper.parse();
            System.out.println(weather);
            JSONMapper jsonMapper = new JSONMapper();
            jsonMapper.buildWeatherJson(weather);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
